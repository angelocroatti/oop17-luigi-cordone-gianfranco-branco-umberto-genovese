# Project Title

Arkanoid

## Built With

* Java FX - Graphical User Interface
* JUnit - Testing

## Authors

Luigi Cordone
Gianfranco Branco
Umberto Genovese

## Instructions

Download and run Arkanoid.jar . 
Please make sure you have a working JRE installed.

Press Bar Space to start to play.
Use mouse to move Paddle.
Touch the brick with the ball to destroy it.
If you destroy all destructible brick you win.

