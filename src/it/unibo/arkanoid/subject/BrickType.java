package it.unibo.arkanoid.subject;


/**
 * Enumeration for Brick Types.
 */
public enum BrickType {

    /**
     * Represent a brick with one lives.
     */
    SIMPLE,

    /**
     * Represents a brick with more lives.
     */
    MULTIPLE,

    /**
     * Represent an indestructible brick.
     */
    INDESTRUCTIBLE,

    /**
     * Represent an empty tile.
     */
    NO_BRICK;

}
